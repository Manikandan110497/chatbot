#start_icon {
    width: 5%;
    border-radius: 100%;
    position: fixed;
    z-index: 5;
    left: 90%;
    top: 90%;
    height: 5%;
}

.main-div-image {
    margin-top: 5px;
    margin-left: 5px;
}

.circle-for-icon {
    height: 33px;
    width: 33px;
    border-radius: 50%;
    margin-top: 2vh;
    display: inline-block;
    /* border: 1px solid #216DB4; */
    margin-left: 1vw;
    background-color:#ff0000    padding-top: 10px;
    padding-left: 8px;
}

.chat-box-container {
    margin-top: -43%;
    height: 500px;
    width: 292px;
    border: 1px solid rgb(0, 0, 0, 0.2);
    margin-left: -19%;
    font-size: 13px;
    position: fixed;
    box-shadow: rgb(149 157 165 / 20%) 1px 8px 24px;
    background-color: #ffffff;
}

.chat-header {
    background-color: #0A83F4;
    height: 63px;
    width: 100%;
    display: flex;
}

.chat-body {
    background-color: white;
    height: 387px;
    width: 100%;
    overflow-y: scroll;
    border: none;
    background-size: cover;
    background-position: center;
}

.chat-footer {
    background-color: white;
    height: 10%;
    width: 100%;
}

.chat-text-box {
    width: 100%;
    height: 53px;
    border: 1px solid rgb(0, 0, 0, 0.2);
}

.chat-text-box:focus:active,
.chat-text-box:focus {
    outline: none;
}

.send-icon {
    position: absolute;
    width: 30px;
    height: 27px;
    z-index: 1;
    left: 250px;
    top: 464px;
}

.self-message {
    width: 100%;
    margin: 0 0 15px;
    display: flex;
    flex-flow: column;
    align-items: flex-end;
}

.self-message-inside-div {
    padding: 6px 10px;
    border-radius: 6px 0 6px 0;
    position: relative;
    background: #0A83F4;
    border: 2px solid rgba(100, 170, 0, .1);
    color: white;
    font-size: 12px;
    max-width: 70%;
    margin-right: 3%;
}

.message-partner {
    margin-left: 3%;
    background: rgba(0, 114, 135, .1);
    border: 2px solid rgba(0, 114, 135, .1);
    align-self: flex-start;
    max-width: 77%;
    padding-left: 5%;
    padding-right: 5%;
}

.image_style {
    width: 100%;
    height: 100%;
}

.image_style {
    width: 100%;
    height: 100%;
}

.style-for-button {
    background-color: transparent;
    border: 0px solid black;
}

.header-image {
    margin-left: 20px;
    width: 35px;
}

.chat-header-bot-name {
    color: white;
    vertical-align: middle;
    margin-top: 21px;
    margin-left: 16px;
    font-size: 18px;
}

.header-image-close {
    margin-left: 100px;
    width: 15px;
    margin-top: 0px;
}

.self-message-wrapper {
    margin-left: 35px;
}